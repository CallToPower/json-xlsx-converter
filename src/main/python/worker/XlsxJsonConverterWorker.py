#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON-XLSX converter.
#

"""JSON-XLSX converter - XlsxJsonConverterWorker"""

import logging
import threading
import json

import pandas as pd

class XlsxJsonConverterWorker(threading.Thread):
    """The converter background worker thread"""

    def init(self,
             infilename='input.xlsx',
             sheetname=None,
             header=None,
             callback=None):
        """Initializes default values because __init__ is not available for threads.

        :param infilename: The name of the in file
        :param sheetname: The name of the excel sheet
        :param headers: The header values
        :param callback: The callback when done processing
        """
        self.infilename = infilename
        self.sheetname = sheetname
        self.header = header
        self.callback = callback
        self.jsondata = ''

        self.error = False

    def read_xlsx(self):
        """Reads in data from an XLSX file"""
        logging.debug('Reading XLSX data...')
        df = pd.read_excel(self.infilename,
                           sheet_name=self.sheetname if self.sheetname else 0,
                           index_col=0,
                           header=self.header,
                           encoding='utf-8')
        return df.to_json(force_ascii=False)

    def run(self):
        """Starts the thread"""
        logging.debug('Starting conversion...')
        try:
            jsondata = self.read_xlsx()
            self.jsondata = json.loads(jsondata)["1" if not self.header else self.header[0]]
            self.error = False
        except Exception as exception:
            logging.exception(exception)
            self.error = True
        finally:
            if self.callback:
                self.callback(self.error, self.jsondata, self.infilename)
