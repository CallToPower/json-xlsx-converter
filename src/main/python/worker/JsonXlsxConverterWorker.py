#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON-XLSX converter.
#

"""JSON-XLSX converter - JsonXlsxConverterWorker"""

import logging
import threading

import pandas as pd

class JsonXlsxConverterWorker(threading.Thread):
    """The converter background worker thread"""

    def init(self,
             outfilename='output.xlsx',
             sheetname='Sheet1',
             jsondata=None,
             headers=None,
             callback=None):
        """Initializes default values because __init__ is not available for threads.

        :param outfilename: The name of the out file
        :param sheetname: The name of the excel sheet
        :param jsondata: The JSON data
        :param headers: The header values
        :param callback: The callback when done processing
        """
        self.outfilename = outfilename
        self.sheetname = sheetname
        self.jsondata = jsondata
        self.headers = headers
        self.callback = callback

        self.error = False

    def write_xlsx(self):
        """Writes the data to an xlsx file"""
        logging.debug('Writing JSON data to xlsx file...')
        df = pd.Series(self.jsondata)
        writer = pd.ExcelWriter(self.outfilename, engine="xlsxwriter")
        df.to_excel(writer,
                    sheet_name=self.sheetname,
                    header=self.headers)
        writer.save()

    def run(self):
        """Starts the thread"""
        logging.debug('Starting conversion...')
        try:
            if self.jsondata:
                self.write_xlsx()
                self.error = False
            else:
                raise Exception('Empty JSON data')
        except Exception as exception:
            logging.exception(exception)
            self.error = True
        finally:
            if self.callback:
                self.callback(self.error, self.outfilename)
