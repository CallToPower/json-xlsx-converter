#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON-XLSX converter.
#

"""JSON-XLSX converter - JsonXlsxConverter"""

import os
import time
import logging
from pathlib import Path

from gui.GUI import GUI

# Logging configuration
logging_loglevel = logging.DEBUG
logging_datefmt = '%d-%m-%Y %H:%M:%S'
logging_format = '[%(asctime)s] [%(levelname)-5s] [%(module)-20s:%(lineno)-4s] %(message)s'
logging_logfile = str(Path.home()) + '/logs/json-xlsx-converter.application-' + time.strftime('%d-%m-%Y-%H-%M-%S') + '.log'

def _initialize_logger():
    basedir = os.path.dirname(logging_logfile)

    if not os.path.exists(basedir):
        os.makedirs(basedir)

    logging.basicConfig(level=logging_loglevel,
                        format=logging_format,
                        datefmt=logging_datefmt)

    handler_file = logging.FileHandler(logging_logfile, mode='w', encoding=None, delay=False)
    handler_file.setLevel(logging_loglevel)
    handler_file.setFormatter(logging.Formatter(fmt=logging_format, datefmt=logging_datefmt))
    logging.getLogger().addHandler(handler_file)

if __name__ == '__main__':
    _initialize_logger()

    logging.info('Starting JSON-XLSX converter...')
    GUI().display()
