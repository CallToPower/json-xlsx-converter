#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON to XLSX converter.
#

"""JSON-XLSX converter - Translations"""

class Translations:
    """The translation helper class"""

    _translations = {
        'APP.VERSION': 'Version',
        'APP.AUTHOR': 'Author',
        'ERR.JSONSAVE.LABEL': 'Did not save file',
        'ERR.JSONSAVE.MSG': 'Error converting data. Did not save to file "{}".',
        'ERR.JSON.INVALID.LABEL': 'JSON data invalid',
        'ERR.JSON.INVALID.MSG': 'The given JSON data are invalid.',
        'ERR.XLSXLOAD.LABEL': 'Did not load file',
        'ERR.XLSXLOAD.MSG': 'Error converting data. Did not load from file "{}".',
        'ERR.XLSXSAVE.LABEL': 'Did not save file',
        'ERR.XLSXSAVE.MSG': 'Error converting data. Did not save to file "{}".',
        'FILES.JSON': 'JSON Files',
        'FILES.XLSX': 'XLSX Files',
        'FILES.ALL': 'All Files',
        'GUI.WINDOW.TITLE': 'JSON-XLSX Converter',
        'GUI.TAB.OUTPUT.JSON': 'XLSX to JSON',
        'GUI.LABEL.OUTPUT.JSON': 'The converted JSON data:',
        'GUI.TAB.INPUT.JSON': 'JSON to XLSX',
        'GUI.LABEL.INPUT.JSON': 'Paste your JSON data or load from file:',
        'GUI.BUTTON.LOADJSONFILE': 'Load from JSON file',
        'GUI.BUTTON.LOADXLSXFILE': 'Load from XLSX file',
        'GUI.BUTTON.SAVEJSONFILE': 'Save to JSON file',
        'GUI.BUTTON.SAVEXLSXFILE': 'Save to XLSX file',
        'GUI.BUTTON.QUIT': 'Quit'
    }

    def get(self, key, default=''):
        """Returns the value for the given key or - if not found - a default value

        :param key: The key to be translated
        :param default: The default value if the key could not be found
        """
        try:
            return self._translations[key]
        except KeyError as exception:
            print('Returning default for key \'{}\': {}'.format(key, exception))
            return default
