#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON-XLSX converter.
#

"""JSON-XLSX converter - GUI"""

import logging

from tkinter import Tk
from tkinter import Menu
from tkinter.messagebox import showinfo

from i18n.Translations import Translations
from gui.MainGUI import MainGUI

class GUI(Tk):
    """The GUI for the JSON-XLSX converter"""

    def __init__(self):
        """Init method"""
        Tk.__init__(self)

        self.translations = Translations()

        self.menu = None
        self.app_menu = None
        self.app_name = 'JSON-XLSX Converter'
        self.app_version = '1.0.0'
        self.app_author = 'Denis Meyer'

    def _show_about_dialog(self):
        logging.debug('Showing about dialog')
        msg = self.app_name + '\n\n{}:\t{}\n{}:\t{}'.format(self.translations.get('APP.VERSION'),
                                                            self.app_version,
                                                            self.translations.get('APP.AUTHOR'),
                                                            self.app_author)
        showinfo(message=msg)

    def _init_menubar(self):
        """Initializes the menu bar"""
        logging.debug('Initializing menu bar')
        self.menu = Menu(self)
        self.app_menu = Menu(self.menu, name='apple')
        self.menu.add_cascade(menu=self.app_menu)
        self.app_menu.add_command(label='About ' + self.app_name, command=self._show_about_dialog)
        self.config(menu=self.menu)

    def display(self):
        """Initializes the GUI and starts the main loop"""
        self._init_menubar()
        MainGUI().display()
