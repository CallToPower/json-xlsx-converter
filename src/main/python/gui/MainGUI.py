#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON-XLSX converter.
#

"""JSON-XLSX converter - MainGUI"""

import sys
import logging

import tkinter as tk
from tkinter import ttk, Frame, scrolledtext, DISABLED, NORMAL
from tkinter.messagebox import showerror
from tkinter.filedialog import asksaveasfilename, askopenfilename
from tkinter import END

from i18n.Translations import Translations
from service.Tools import pp_json, parse_json_from_file, parse_json_from_str
from worker.JsonXlsxConverterWorker import JsonXlsxConverterWorker
from worker.XlsxJsonConverterWorker import XlsxJsonConverterWorker

def clear_print(scrolledtext_ref):
    """Clears the given output textarea

    :param scrolledtext_ref: The reference to the scolledtext UI element
    """
    scrolledtext_ref.configure(state=NORMAL)
    scrolledtext_ref.delete('1.0', END)
    scrolledtext_ref.configure(state=DISABLED)

def print_json(scrolledtext_ref, strtoprint):
    """Appends the given string to the output textarea

    :param scrolledtext_ref: The reference to the scolledtext UI element
    :param strtoprint: The string to be appended to the element
    """
    scrolledtext_ref.configure(state=NORMAL)
    scrolledtext_ref.insert(tk.INSERT, strtoprint + '\n')
    scrolledtext_ref.configure(state=DISABLED)

class MainGUI(Frame):
    """The main GUI for the JSON-XLSX converter"""

    _pad_x = 8
    _pad_y = 4
    _display_quit_button = False

    def __init__(self):
        """Init method"""
        Frame.__init__(self)

        self.translations = Translations()

        self._text_scrolled = []
        self._buttons = []
        self.tabcontrol = None
        self.tab1 = None
        self.tab2 = None
        self.labelframe1 = None
        self.labelframe2 = None
        self.text_scrolled_json = None
        self.text_scrolled_xlsx = None
        self.button_loadjsonfile = None
        self.button_loadxlsxfile = None
        self.button_savejsonfile = None
        self.button_savexlsxfile = None
        self.button_quit = None

    def display(self):
        """Initializes the GUI and starts the main loop"""
        self._initgui()
        self.mainloop()

    def _doquit(self):
        """Destroys the window and quits"""
        self.quit()
        self.destroy()
        sys.exit(0)

    def _enable(self):
        """Enables all buttons, inputs, etc."""
        for ts in self._text_scrolled:
            ts.configure(state=NORMAL)
        for but in self._buttons:
            but.config(state=NORMAL)

    def _disable(self):
        """Disables all buttons, inputs, etc."""
        for ts in self._text_scrolled:
            ts.configure(state=DISABLED)
        for but in self._buttons:
            but.config(state=DISABLED)

    def _callback_jsonextracted(self, error, jsondata, filename):
        """Callback for JSON data extraction

        :param error: Boolean flag whether an error occurred
        :param jsondata: The JSON data
        :param filename: The name of the save file
        """
        logging.debug('JSON extracted callback...')
        try:
            if error:
                showerror(self.translations.get('ERR.XLSXLOAD.LABEL'),
                          self.translations.get('ERR.XLSXLOAD.MSG')
                          .format(filename))
            else:
                print_json(self.text_scrolled_xlsx, pp_json(jsondata))
        finally:
            self._enable()

    def _callback_xlsxfilesave(self, error, filename):
        """Callback for XLSX file saving

        :param error: Boolean flag whether an error occurred
        :param filename: The name of the save file
        """
        logging.debug('XLSX file save callback...')
        try:
            if error:
                showerror(self.translations.get('ERR.XLSXSAVE.LABEL'),
                          self.translations.get('ERR.XLSXSAVE.MSG')
                          .format(filename))
        finally:
            self._enable()

    def _load_from_json_file(self):
        """When the 'Load from file' button has been clicked"""
        logging.debug('Loading from JSON file...')
        self._disable()
        try:
            filename_json = askopenfilename(
                filetypes=((self.translations.get('FILES.JSON'), '*.json'),
                           (self.translations.get('FILES.ALL'), '*.*')))
            if filename_json:
                logging.debug('Loading JSON from file "{}"'.format(filename_json))
                clear_print(self.text_scrolled_json)
                with open(filename_json, encoding='utf-8') as fin:
                    jsondata = parse_json_from_file(fin)
                    if not jsondata:
                        showerror(self.translations.get('ERR.JSON.INVALID.LABEL'),
                                  self.translations.get('ERR.JSON.INVALID.MSG'))
                    else:
                        print_json(self.text_scrolled_json, pp_json(jsondata))
        except Exception as exception:
            logging.exception(exception)
        finally:
            self._enable()

    def _load_from_xlsx_file(self):
        """When the 'Load from file' button has been clicked"""
        logging.debug('Loading from XLSX file...')
        self._disable()
        try:
            filename_xlsx = askopenfilename(
                filetypes=((self.translations.get('FILES.XLSX'), '*.xlsx'),
                           (self.translations.get('FILES.ALL'), '*.*')))
            if filename_xlsx:
                logging.debug('Loading XLSX from file "{}"'.format(filename_xlsx))
                clear_print(self.text_scrolled_xlsx)
                worker = XlsxJsonConverterWorker()
                worker.init(callback=self._callback_jsonextracted,
                            infilename=filename_xlsx)
                worker.start()
            else:
                self._enable()
        except Exception as exception:
            logging.exception(exception)
            self._enable()

    def _save_to_json_file(self):
        """When the 'Save to file' button has been clicked"""
        logging.debug('Saving to JSON file...')
        try:
            self._disable()
            jsondata = parse_json_from_str(self.text_scrolled_xlsx.get(1.0, END))
            if not jsondata:
                showerror(self.translations.get('ERR.JSON.INVALID.LABEL'),
                          self.translations.get('ERR.JSON.INVALID.MSG'))
            else:
                fname = asksaveasfilename()
                if fname:
                    if not fname.endswith('.json'):
                        fname += '.json'
                    logging.debug('Saving data to JSON file "{}"'.format(fname))
                    with open(fname, 'w', encoding='utf-8') as text_file:
                        print(pp_json(jsondata), file=text_file)
        except Exception as exception:
            logging.exception(exception)
        finally:
            self._enable()

    def _save_to_xlsx_file(self):
        """When the 'Save to file' button has been clicked"""
        logging.debug('Saving to XLSX file...')
        self._disable()
        jsondata = parse_json_from_str(self.text_scrolled_json.get(1.0, END))
        if not jsondata:
            self._enable()
            showerror(self.translations.get('ERR.JSON.INVALID.LABEL'),
                      self.translations.get('ERR.JSON.INVALID.MSG'))
        else:
            filename_xlsx = asksaveasfilename()
            if filename_xlsx:
                if not filename_xlsx.endswith('.xlsx'):
                    filename_xlsx += '.xlsx'
                logging.debug('Saving data to XLSX file "{}"'.format(filename_xlsx))
                worker = JsonXlsxConverterWorker()
                worker.init(callback=self._callback_xlsxfilesave,
                            jsondata=jsondata,
                            outfilename=filename_xlsx)
                worker.start()
            else:
                self._enable()

    def _center(self):
        """Centers the window"""
        logging.debug('Centering application')
        self.master.update_idletasks()
        width = self.master.winfo_width()
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width
        height = self.master.winfo_height()
        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = self.master.winfo_screenwidth() // 2 - win_width // 2
        y = self.master.winfo_screenheight() // 2 - win_height // 2
        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.resizable(width=False, height=False)
        self.master.deiconify()

    def _add_to_grid(self):
        """Adds the UI components to the grid"""
        logging.debug('Adding components to grid')
        currrow = 0
        currcolumn = 0
        rowspan = 1
        colspan = 1
        self.tabcontrol.grid(row=currrow,
                             column=currcolumn,
                             rowspan=rowspan,
                             columnspan=colspan,
                             sticky=tk.N+tk.E+tk.S+tk.W)
        currrow += 1
        if self._display_quit_button:
            self.button_quit.grid(row=currrow,
                                  column=currcolumn,
                                  rowspan=rowspan,
                                  columnspan=colspan,
                                  sticky=tk.N+tk.E+tk.S+tk.W)

        self.button_loadjsonfile.grid(row=currrow,
                                      column=currcolumn,
                                      rowspan=rowspan,
                                      columnspan=colspan,
                                      sticky=tk.N+tk.E+tk.S+tk.W)

        currrow += 1
        self.button_savexlsxfile.grid(row=currrow,
                                      column=currcolumn,
                                      rowspan=rowspan,
                                      columnspan=colspan,
                                      sticky=tk.N+tk.E+tk.S+tk.W)

        self.button_loadxlsxfile.grid(row=currrow,
                                      column=currcolumn,
                                      rowspan=rowspan,
                                      columnspan=colspan,
                                      sticky=tk.N+tk.E+tk.S+tk.W)

        currrow += 1
        self.button_savejsonfile.grid(row=currrow,
                                      column=currcolumn,
                                      rowspan=rowspan,
                                      columnspan=colspan,
                                      sticky=tk.N+tk.E+tk.S+tk.W)

        sticky = tk.N+tk.E+tk.W
        self.labelframe1.grid(column=0,
                              row=0,
                              padx=self._pad_x,
                              pady=self._pad_y,
                              sticky=sticky+tk.S)
        self.labelframe2.grid(column=0,
                              row=0,
                              padx=self._pad_x,
                              pady=self._pad_y,
                              sticky=sticky+tk.S)

        currrow = 0
        currcolumn = 0
        rowspan = 3
        colspan = 3
        sticky = tk.N+tk.E+tk.S+tk.W
        self.text_scrolled_json.grid(row=currrow,
                                     column=currcolumn,
                                     rowspan=rowspan,
                                     columnspan=colspan,
                                     sticky=sticky)
        self.text_scrolled_xlsx.grid(row=currrow,
                                     column=currcolumn,
                                     rowspan=rowspan,
                                     columnspan=colspan,
                                     sticky=sticky)

    def _init_scrolledtext(self):
        """Initializes the scrolledtext areas"""
        logging.debug('Initializing text areas')
        scrol_w = 100
        scrol_h = 20
        self.text_scrolled_json = scrolledtext.ScrolledText(self.labelframe1,
                                                            width=scrol_w,
                                                            height=scrol_h,
                                                            wrap=tk.WORD)
        self.text_scrolled_json.configure(state="disabled")
        self._text_scrolled.append(self.text_scrolled_json)

        scrol_w = 100
        scrol_h = 20
        self.text_scrolled_xlsx = scrolledtext.ScrolledText(self.labelframe2,
                                                            width=scrol_w,
                                                            height=scrol_h,
                                                            wrap=tk.WORD)
        self.text_scrolled_xlsx.configure(state="disabled")
        self._text_scrolled.append(self.text_scrolled_xlsx)

    def _init_buttons(self):
        """Initializes the buttons"""
        logging.debug('Initializing buttons')
        self.button_loadjsonfile = ttk.Button(self.tab1,
                                              text=self.translations.get('GUI.BUTTON.LOADJSONFILE'),
                                              command=self._load_from_json_file)
        self._buttons.append(self.button_loadjsonfile)
        self.button_savexlsxfile = ttk.Button(self.tab1,
                                              text=self.translations.get('GUI.BUTTON.SAVEXLSXFILE'),
                                              command=self._save_to_xlsx_file)
        self._buttons.append(self.button_savexlsxfile)
        self.button_loadxlsxfile = ttk.Button(self.tab2,
                                              text=self.translations.get('GUI.BUTTON.LOADXLSXFILE'),
                                              command=self._load_from_xlsx_file)
        self._buttons.append(self.button_loadxlsxfile)
        self.button_savejsonfile = ttk.Button(self.tab2,
                                              text=self.translations.get('GUI.BUTTON.SAVEJSONFILE'),
                                              command=self._save_to_json_file)
        self._buttons.append(self.button_savejsonfile)
        if self._display_quit_button:
            self.button_quit = ttk.Button(self,
                                          text=self.translations.get('GUI.BUTTON.QUIT'),
                                          command=self._doquit)
            self._buttons.append(self.button_quit)

    def _init_tabs(self):
        """Initializes the UI tabs"""
        logging.debug('Initializing tabs')
        self.tabcontrol = ttk.Notebook(self)
        self.tab1 = ttk.Frame(self.tabcontrol)
        self.tab2 = ttk.Frame(self.tabcontrol)
        self.tabcontrol.add(self.tab1,
                            text=self.translations.get('GUI.TAB.INPUT.JSON'))
        self.tabcontrol.add(self.tab2,
                            text=self.translations.get('GUI.TAB.OUTPUT.JSON'))
        self.labelframe1 = ttk.LabelFrame(self.tab1,
                                          text=self.translations.get('GUI.LABEL.INPUT.JSON'))
        self.labelframe2 = ttk.LabelFrame(self.tab2,
                                          text=self.translations.get('GUI.LABEL.OUTPUT.JSON'))

        self.tabcontrol.rowconfigure(0, weight=1)
        self.tabcontrol.columnconfigure(0, weight=1)
        self.tab1.rowconfigure(0, weight=1)
        self.tab1.columnconfigure(0, weight=1)
        self.tab2.rowconfigure(0, weight=1)
        self.tab2.columnconfigure(0, weight=1)

        self.labelframe1.rowconfigure(0, weight=1)
        self.labelframe1.rowconfigure(1, weight=1)
        self.labelframe1.columnconfigure(0, weight=1)
        self.labelframe1.columnconfigure(1, weight=1)
        self.labelframe2.rowconfigure(0, weight=1)
        self.labelframe2.rowconfigure(1, weight=1)
        self.labelframe2.columnconfigure(0, weight=1)
        self.labelframe2.columnconfigure(1, weight=1)

    def _configure_properties(self):
        """Configures frame properties"""
        logging.debug('Configuring properties')
        self.master.title(self.translations.get('GUI.WINDOW.TITLE'))
        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)
        self.grid(sticky=tk.E+tk.S+tk.W, padx=self._pad_x, pady=self._pad_y)

    def _register_commands(self):
        """Registers some commands on special events, e.g. 'exit'"""
        logging.debug('Registering commands')
        self.master.createcommand('exit', self._doquit)

    def _initgui(self):
        """Initializes the GUI"""
        self._register_commands()
        self._configure_properties()
        self._init_tabs()
        self._init_buttons()
        self._init_scrolledtext()
        self._add_to_grid()
        self._center()
        self._enable()
