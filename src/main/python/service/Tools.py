#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the JSON to XLSX converter.
#

"""JSON-XLSX converter - Tools"""

import logging
import json

def pp_json(jsondata):
    """Pretty-prints JSON data

    :param jsondata: The JSON data to be printed
    """
    strprnt = '{'
    cnt = 0
    format_str = '{}\n{}"{}": {}'
    indent = '    '
    for key in jsondata:
        strprnt += format_str.format(',' if cnt > 0 else '',
                                     indent,
                                     key,
                                     json.dumps(jsondata[key], ensure_ascii=False))
        cnt += 1
    strprnt += '\n}'
    return strprnt

def parse_json_from_file(jsonfile):
    """Checks whether the given file is valid JSON
       and returns the parsed JSON object if it is

    :param jsonfile: The JSON file to be parsed
    """
    try:
        return json.load(jsonfile)
    except ValueError as ve:
        logging.exception(ve)
        return False

def parse_json_from_str(jsonstr):
    """Checks whether the given string is valid JSON
       and returns the parsed JSON object if it is

    :param jsonstr: The JSON string to be parsed
    """
    try:
        return json.loads(jsonstr)
    except ValueError as ve:
        logging.exception(ve)
        return False
