# JSON-XLSX Converter

Graphical application that converts data between JSON and XLSX.

* JSON to XLSX
* XLSX to JSON

![Main GUI](screenshots/main_ui.png "Main GUI")

Copyright 2017-2018 Denis Meyer.

## Prerequisites

* Python 3
* Windows
  * Install NSIS - http://nsis.sourceforge.net
  * Add Python to PATH variable in environment
  * Add NSIS to PATH variable in environment

## Usage

* Start shell
  * Windows
    * Start shell as administrator
    * `Set-ExecutionPolicy Unrestricted -Force`
* Create a virtual environment
  * `python -m venv venv`
* Activate the virtual environment
  * Mac/Linux
    * `source venv/bin/activate`
  * Windows
    * `.\venv\scripts\activate`
* Install the required libraries
  * `pip install -r requirements.txt`
* Run the app
  * `python -m fbs run`

## Shipping

* Freeze the app (create an executable)
  * `python -m fbs freeze`
* Build an installer (create an installer)
  * `python -m fbs installer`

## TODO

* Exchange pandas 0.20.0 with 0.22.0 when the following ticket is fixed:
  * https://stackoverflow.com/questions/47318119/no-module-named-pandas-libs-tslibs-timedeltas-in-pyinstaller
